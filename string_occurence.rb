# This is a program to count the occurence of each characters in string and return the output in hash
def count_occurence(input)

		output_hash=Hash.new(0)
		input.each_char do|char|
			
			output_hash[char]+=1 if char =~/[A-Za-z]/
			
		end
		return output_hash
end

#Array of input
input=["","1","hello world","password123","@#goodbye*%"]	
input.each{|i| puts count_occurence(i)}
